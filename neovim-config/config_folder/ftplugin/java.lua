local bundles = {
  vim.fn.glob("/Users/i866859/.local/share/nvim/mason/packages/java-debug-adapter/extension/server/com.microsoft.java.debug.plugin-*.jar", 1),
};

vim.list_extend(bundles, vim.split(vim.fn.glob("/Users/i866859/.local/share/nvim/mason/packages/java-test/extension/server/*.jar", 1), "\n\t"))

local config = {
    cmd = {'/Users/i866859/.local/share/nvim/mason/bin/jdtls'},
    root_dir = vim.fs.dirname(vim.fs.find({'gradlew', '.git', 'mvnw'}, { upward = true })[1]),
	init_options = {
		bundles = bundles
	}
}
require('jdtls').start_or_attach(config)


vim.keymap.set('n', '<leader>b', '<cmd>DapToggleBreakpoint<cr>')
vim.keymap.set('n', '<leader>s', function()
  local dap = require('dap')
  local widgets = require('dap.ui.widgets')
  dap.continue()
  local scopes_sidebar = widgets.sidebar(widgets.scopes)
  local frames_sidebar = widgets.sidebar(widgets.frames)
  scopes_sidebar.open({heigth = 20, width = 100}, 'aboveleft split')
  frames_sidebar.open({heigth = 20, width = 100}, 'belowleft split')
end)
vim.keymap.set('n', '<leader>n', '<cmd>DapStepOver<cr>')
vim.keymap.set('n', '<leader>o', '<cmd>DapStepOut<cr>')
vim.keymap.set('n', '<leader>i', '<cmd>DapStepIn<cr>')
vim.keymap.set('n', '<leader>t', '<cmd>DapTerminate<cr>')
vim.keymap.set('n', '<leader>r', '<cmd>DapToggleRepl<cr>')
vim.keymap.set('n', '<leader>l', '<cmd>DapShowLog<cr>')
vim.keymap.set('n', '<leader>c', function()
  require('dap').run_to_cursor()
end)
vim.keymap.set({'n', 'v'}, '<Leader>dh', function()
  local widgets = require('dap.ui.widgets')
  widgets.hover()
end)
vim.keymap.set({'n', 'v'}, '<Leader>dp', function()
  local widgets = require('dap.ui.widgets')
  widgets.preview()
end)
vim.keymap.set('n', '<Leader>df', function()
  local widgets = require('dap.ui.widgets')
  widgets.centered_float(widgets.frames)
end)
vim.keymap.set('n', '<Leader>ds', function()
  local widgets = require('dap.ui.widgets')
  widgets.centered_float(widgets.scopes)
end)

