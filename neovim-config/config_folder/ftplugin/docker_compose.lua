-- Set up lspconfig.
local capabilities = require('cmp_nvim_lsp').default_capabilities()

-- Setup language servers.
local lspconfig = require('lspconfig')

lspconfig.docker_compose_language_service.setup {
	capabilities = capabilities
}
