require("translate").setup({
	output = {
		float = {
			-- max_width of float window
			max_width = 40,
			-- max_height of float window
			max_height = 5,
			-- whether close float window on cursor move
			close_on_cursor_move = true,
			-- key to enter float window
			enter_key = "T",
		},
	},
	translate = {
		{
			-- use :TransToBR to start this job
			cmd = "TransToEN",
			-- shell command
			-- translate-shell is used here
			command = "trans",
			-- shell command args
			args = function(trans_source)
				return {
					"-b",
					"-e",
					"google",
					"-t",
					"pt-BR",
					trans_source,
				}
			end,
			-- how to get translate source
			-- selection | input | clipboard
			input = "selection",
			-- how to output translate result
			-- float_win | notify | clipboard | insert
			output = { "insert" },
			-- format output
			---@type fun(output: string): string
			format = function(output)
				return output
			end,
		},
	},
})
vim.keymap.set("v", "<C-t>1", "<cmd>TransToEN<CR>")

vim.api.nvim_create_autocmd("Filetype", {
	pattern = "tex",
	callback = function()
		vim.keymap.set("n", "<C-t>1", function()
			vim.fn.execute('!trans -i "' .. vim.fn.expand('%') .. '" -t en -show-original n -b -no-autocorrect -show-alternatives n -no-warn -o "' .. vim.fn.expand('%:r') .. '_en.tex"')
		end)
	end
})
vim.opt.termguicolors = true
