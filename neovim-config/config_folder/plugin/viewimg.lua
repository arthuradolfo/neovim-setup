vim.api.nvim_create_autocmd("BufEnter", {
	pattern = {"*.jpeg", "*.jpg", "*.png"},
	command = "terminal timg '%'",
})
