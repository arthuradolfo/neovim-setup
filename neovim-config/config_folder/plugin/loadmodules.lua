function exists(file)
   local ok, err, code = os.rename(file, file)
   if not ok then
      if code == 13 then
         -- Permission denied, but it exists
         return true
      end
   end
   return ok, err
end

if exists(vim.fn.getcwd() .. '/.nvim-plugins/') then
	local dircmd = "find .nvim-plugins/ -type f -print" -- default to Unix
	if string.sub(package.config,1,1) == '\\' then
		-- Windows
		dircmd = "dir .nvim-plugins/ /b/s"
	end

	os.execute(dircmd .. " > zzfiles")

	for f in io.lines("zzfiles") do
		if f:sub(-4) == ".lua" then
			dofile(vim.fn.getcwd() .. '/' .. f)
		end
	end

	os.execute('rm zzfiles')

end
