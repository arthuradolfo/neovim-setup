tex_artifacts = { '.aux', '.bbl', '.bcf', '.blg', '.fdb_latexmk', '.fls', '.hd', '.log', '.out', '.synctex.gz', '.run.xml', '.pdf' }

vim.api.nvim_create_autocmd("Filetype", {
	pattern = "tex",
	callback = function() 
		-- KEY MAPPING
		--	-- ITALIC MODE
		vim.keymap.set('i', '<C-d>', '\\textit{}<esc>i')
		vim.keymap.set('v', '<C-d>', 'xi\\textit{<esc>pa}<esc>')
		
		--	-- BOLD MODE
		vim.keymap.set('i', '<C-b>', '\\textbf{}<esc>i')
		vim.keymap.set('v', '<C-b>', 'xi\\textbf{<esc>pa}<esc>')

		--	-- SECTIONS CREATION
		vim.keymap.set('i', '<C-s>1', '\\section{}<esc>i')
		vim.keymap.set('i', '<C-s>2', '\\subsection{}<esc>i')
		vim.keymap.set('i', '<C-s>3', '\\subsubsection{}<esc>i')
		vim.keymap.set('i', '<C-s>4', '\\subsubsubsection{}<esc>i')
		vim.keymap.set('i', '<C-s>5', '\\subsubsubsubsection{}<esc>i')
		vim.keymap.set('i', '<C-s>6', '\\subsubsubsubsubsection{}<esc>i')

		--	-- ENVIRONEMNT CREATION
		vim.keymap.set('i', '<C-e>', '\\begin{nd}\n\n\n\n\\end{nd}<esc>kka\t')
		vim.keymap.set('i', '<C-l>a', '\\begin{enumerate}[label=(\\alph*)]\n\n\n\n\\end{nd}<esc>kka\t\\item ')
		vim.keymap.set('i', '<C-l>n', '\\begin{enumerate}\n\n\n\n\\end{nd}<esc>kka\t\\item ')
		vim.keymap.set('i', '<C-l>i', '\n\n\t\\item ')

		-- -- COMPILE PDF
		vim.keymap.set('n', '<C-c>1', ':VimtexCompile<enter>')

		-- NVIM LEAVE
		vim.api.nvim_create_autocmd("QuitPre", {
			pattern = "*",
			callback = function()
				-- Wipe out TeX artifacts
				for i=1, #tex_artifacts do
					vim.fn.execute('!rm "' .. vim.fn.expand("%:r") .. tex_artifacts[i] .. '"')
				end
			end,
		})
	end, 
})

