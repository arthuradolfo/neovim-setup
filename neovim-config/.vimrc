:let $TEXMFHOME='~/texmf'
:set number
:set relativenumber
:set autoindent
:set tabstop=4
:set shiftwidth=4
:set smarttab
:set softtabstop=4
:set mouse=a

call plug#begin()

Plug 'dylanaraps/fff.vim' 
Plug 'preservim/nerdtree'
Plug 'vim-airline/vim-airline'
Plug 'tpope/vim-surround'
Plug 'tpope/vim-commentary'
Plug 'ap/vim-css-color'
Plug 'rafi/awesome-vim-colorschemes'
Plug 'ryanoasis/vim-devicons'
Plug 'terryma/vim-multiple-cursors'
Plug 'preservim/tagbar'
Plug 'iamcco/markdown-preview.nvim', { 'do': 'cd app && npx --yes yarn install' }
Plug 'nvim-lua/plenary.nvim'
Plug 'tanvirtin/vgit.nvim'
Plug 'puremourning/vimspector'
Plug 'ctrlpvim/ctrlp.vim'
Plug 'mileszs/ack.vim'
Plug 'Yggdroot/indentLine'
Plug 'lervag/vimtex'
Plug 'Darazaki/indent-o-matic'
Plug 'rhysd/vim-grammarous'
Plug 'rcarriga/nvim-notify'
Plug 'niuiic/core.nvim'
Plug 'niuiic/translate.nvim'
Plug 'mracos/mermaid.vim'
Plug 'nvim-lua/plenary.nvim'
Plug 'nvim-telescope/telescope.nvim', { 'tag': '0.1.5' }
Plug 'ahmedkhalf/project.nvim'
Plug 'JuliaEditorSupport/julia-vim'
Plug 'williamboman/mason.nvim'                                                  
Plug 'williamboman/mason-lspconfig.nvim'                                        
Plug 'neovim/nvim-lspconfig'
Plug 'hrsh7th/cmp-nvim-lsp'
Plug 'hrsh7th/cmp-buffer'
Plug 'hrsh7th/cmp-path'
Plug 'hrsh7th/cmp-cmdline'
Plug 'hrsh7th/nvim-cmp'
Plug 'hrsh7th/cmp-vsnip'
Plug 'hrsh7th/vim-vsnip'
Plug 'micangl/cmp-vimtex'
Plug 'kdheepak/cmp-latex-symbols'
Plug 'mfussenegger/nvim-dap'
Plug 'eatgrass/maven.nvim'
Plug 'mfussenegger/nvim-jdtls'
Plug 'catppuccin/nvim', { 'as': 'catppuccin' }
Plug 'zefei/vim-wintabs'
Plug 'zefei/vim-wintabs-powerline'

call plug#end()

filetype on
filetype plugin indent on
syntax enable

let g:vimtex_view_method = 'mupdf'
let g:vimtex_view_general_viewer = 'okular'
let g:vimtex_view_general_options = '--unique file:@pdf\#src:@line@tex'
let g:vimtex_compiler_method = 'latexmk'

let maplocalleader = ","
"" ENDOFLATEX

"" MAVEN
lua << EOF
require('maven').setup({
  executable = "mvn", -- `mvn` should be in your `PATH`, or the path to the maven exectable, for example `./mvnw`
  cwd = nil, -- work directory, default to `vim.fn.getcwd()`
  settings = nil, -- specify the settings file or use the default settings
  commands = { -- add custom goals to the command list
    { cmd = { "clean", "compile" }, desc = "clean then compile" },
  },
})
EOF
"" END MAVEN

"" PROJECT.NVIM
lua << EOF
  require("project_nvim").setup {
    show_hidden = true,
  }
EOF
"" END PROJECT.NVIM

"" TELESCOPE
lua << EOF
  require('telescope').load_extension('projects')
EOF

nnoremap <leader>ff <cmd>Telescope find_files<cr>
nnoremap <leader>fg <cmd>Telescope live_grep<cr>
nnoremap <leader>fb <cmd>Telescope buffers<cr>
nnoremap <leader>fh <cmd>Telescope help_tags<cr>
nnoremap <leader>p <cmd>Telescope projects<cr> 
"" END OF TELESCOPE

"" NERDTREE
nnoremap <C-f> :NERDTreeFocus<CR>
nnoremap <C-n> :NERDTree<CR>
nnoremap <C-t> :NERDTreeToggle<CR>
let NERDTreeShowHidden=1
"" END NERDTREE

"" PERSONAL MAPPINGS
:set list lcs=tab:\|\ 

nnoremap <leader>vimrc <cmd>edit ~/.vimrc<cr>
tnoremap <Esc> <C-\><C-n>
"" END PERSONAL MAPPINGS

"" TAGBAR
nmap <F7> :TagbarToggle<CR>
"" END TAGBAR

:set completeopt-=preview

"" COLOR THEME
:colorscheme catppuccin-mocha

if (exists('+colorcolumn'))
	set colorcolumn=80
	highlight ColorColumn ctermbg=Black
endif
"" END COLOR THEME

"" AIRLINE
let g:airline_powerline_fonts = 1

if !exists('g:airline_symbols')
    let g:airline_symbols = {}
endif

" airline symbols
let g:airline_left_sep = ''
let g:airline_left_alt_sep = ''
let g:airline_right_sep = ''
let g:airline_right_alt_sep = ''
let g:airline_symbols.branch = ''
let g:airline_symbols.readonly = ''
let g:airline_symbols.linenr = ''
let g:airline#extensions#tabline#enabled = 1
let g:airline#extensions#tabline#fnamemod = ':t'
"" END AIRLINE

inoremap <expr> <Tab> pumvisible() ? coc#_select_confirm() : "<Tab>"

"" VIMSPECTOR
let g:vimspector_enable_mappings = 'HUMAN'
"" END VIMSPECTOR

"" GRAMMAROUS
let g:grammarous#jar_url = 'https://www.languagetool.org/download/LanguageTool-5.9.zip'
let g:grammarous#disabled_rules = {
	\ '*' : ['PT_WORDINESS_REPLACE_A_PRIORI', 'PT_WORDINESS_REPLACE_A_POSTERIORI']
\ }
"" END GRAMMAROUS

"" VGIT
lua << EOF
	require('vgit').setup()
EOF
"" END VGIT


