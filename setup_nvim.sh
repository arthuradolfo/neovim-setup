#!/bin/bash
export TEXMFHOME=~/texmf
is_macos=0
is_archos=0

set -e

# Download and install neovim
install_nvim() {
	if [[ $is_macos == 1 ]]; then
		brew install neovim ripgrep fff npm nodejs yarn
	elif [[ $is_archos  ]]; then
		sudo pacman -Syu neovim ripgrep fff npm nodejs yarn
	fi
}

# Download and install fitch.sty
install_fitch() {
	mkdir -p $TEXMFHOME/tex/latex/local
	curl https://raw.githubusercontent.com/OpenLogicProject/fitch/main/fitch.sty > $TEXMFHOME/tex/latex/local/fitch.sty
	texhash $TEXMFHOME/tex/latex/local
}

# Download and install latex
install_latex() {
	if [[ $is_macos == 1 ]]; then
		brew install texlive biber
		install_fitch
	elif [[ $is_archos == 1 ]]; then
		sudo pacman -Syu texlive-latex biber
		install_fitch
	fi
}

# Check OS
if [[ "$OSTYPE" == "darwin"* ]]; then
	is_macos=1
elif [[ "$OSTYPE" == "linux-gnu"* ]]; then
	if [[ "$(cat /etc/os-release | grep "^NAME=\"Arch Linux\"$")" == "NAME=\"Arch Linux\"" ]]; then
		is_archos=1
	else
		echo "This script works only on MacOS and Arch Linux distro."; exit -1;
	fi
fi

# Remove old .vimrc and link repository's .vimrc
rm ~/.vimrc
ln -s $(pwd)/neovim-config/.vimrc ~/.vimrc
echo "Linked .vimrc successfully."

# Copy neovim's .config folder sctructure and files
rm -rf ~/.config/nvim
ln -s $(pwd)/neovim-config/config_folder/ ~/.config/nvim
echo "Linked .config/nvim folder successfully."

# Download and install LaTeX related libraries
install_latex
echo "Installed latex successfully."
