$Env:TexmfHome="C:\texlive\texmf-local"
$Env:Vimrc="$Env:HOMEPATH\AppData\Local\nvim"

# Download and install neovim
function install_nvim {
	winget install neovim ripgrep fff npm nodejs yarn
}

# Download and install fitch.sty
function install_fitch {
	mkdir -p "$Env:TexmfHome\tex\latex\local"
	$(curl https://raw.githubusercontent.com/OpenLogicProject/fitch/main/fitch.sty).Content > "$Env:TexmfHome\tex\latex\local\fitch.sty"
	texhash "$Env:TexmfHome\tex\latex\local"
}

# Download and install latex
function install_latex {
	# No winget packages for texlive and biber
	install_fitch
}

# Remove old .vimrc and link repository's .vimrc
rm "$Env:HOMEPATH\.vimrc"
[IO.File]::WriteAllLines("$(pwd)\neovim-config\win.vimrc", ((cat "$(pwd)\neovim-config\.vimrc") -replace "plug#begin\(\)","plug#begin('~/AppData/Local/nvim/plugged')"))
cmd /c mklink "$Env:HOMEPATH\.vimrc" "$(pwd)\neovim-config\win.vimrc"
echo "Linked .vimrc successfully."

# Copy neovim's .config folder sctructure and files
rm -r "$Env:Vimrc\plugin"
rm -r "$Env:Vimrc\ftplugin"
rm "$Env:Vimrc\init.vim"
cmd /c mklink /d "$Env:Vimrc\plugin" "$(pwd)\neovim-config\config_folder\plugin"
cmd /c mklink /d "$Env:Vimrc\ftplugin" "$(pwd)\neovim-config\config_folder\ftplugin"
cmd /c mklink "$Env:Vimrc\init.vim" "$(pwd)\neovim-config\config_folder\init.vim"
echo "Linked .config/nvim folder successfully."

# Download and install LaTeX related libraries
install_latex
echo "Installed latex successfully."
